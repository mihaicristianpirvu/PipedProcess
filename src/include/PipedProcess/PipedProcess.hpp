/* PipedProcess.h - Class for the a new piped process. Stdin will be writen by main process and data written to stdout
 * will be captured by the main process  */

#ifndef PIPED_PROCESS_H
#define PIPED_PROCESS_H

#include <assert.h>
#include <cstring>
#include <chrono>
#include <exception>
#include <functional>
#include <iostream>
#include <PipedProcess/Pipe.hpp>
#include <thread>

#ifdef __linux__
#include <fcntl.h>
#include <signal.h>
#include <sys/prctl.h>

#ifndef _GNU_SOURCE 
	#define _GNU_SOURCE
#endif /* _GNU_SOURCE */

typedef int PipeFlags;
#elif _WIN32
typedef DWORD PipeFlags;
#endif /* __linux__ */

struct piped_process_exception : public std::runtime_error {
	piped_process_exception(const std::string &msg, int return_value = 0) : std::runtime_error(msg),
		return_value(return_value) {}

	int return_value = 0;
};

struct out_of_time : public piped_process_exception {
	out_of_time(const std::string  msg, int return_value = 0) : piped_process_exception(msg, return_value) {}
};

struct invalid_read : public piped_process_exception {
	invalid_read(const std::string &msg, int return_value = 0) : piped_process_exception(msg, return_value) {}
};

class PipedProcess {
public:
	struct ReadFlags {
		ReadFlags(int x) : value(x) {}
		static constexpr int NonBlocking = 0x1;			/* Blocking flag */
		static constexpr int NoSpecialChars = 0x2;		/* Special characters */
		static constexpr int ThrowExceptions = 0x4;		/* Throws exceptions or not */
		static constexpr int Timer = 0x8;				/* The read will return after a specified time */

		bool operator& (const int x) const {
			return this->value & x;
		}

	private:
		int value;
	};

	PipedProcess();
	void setArguments(int, char **);
	/* Writes the word to the write pipe */
	int write(const std::string &);
	/* Checks if the string is already ended in a new line and if not, adds a \n and calls write */
	int writeln(const std::string &);
	/* Reads a word from the rewad pipe. Aditional flags can be given, such as blocking, removal of special characters
	 * at beginning and end of the word, whether it should throw expcetions or not and a timer before abandoning. */
	int read(std::string &, size_t, PipedProcess::ReadFlags = static_cast<PipedProcess::ReadFlags>(0), 
		const std::chrono::milliseconds & = PipedProcess::defaultInterval,
		const std::chrono::milliseconds & = PipedProcess::defaultDelay); 
	 /* Function that spawns a new process based on the argc and argv, creates a pipe and then the parent process 
	  * returns to it's normal state, and the new process starts its execution. */
	void startProcess();
	 /* Prints the name of the process and the arguments before spawning the child */
	void printSpawning();
	~PipedProcess() {}
private:
	struct Arguments {
		char **argv;
		int argc;
	};

	/* Protocol messages used by the parent process and the piped process */
	struct CommunicationProtocol {
		constexpr static const char *handshakeInitialMessage = "MAIN";
		constexpr static const char *handshakeAcceptMessage = "ACCEPT";
		constexpr static const char *ACKMessage = "OK";
	};

	/* Method called after startProcess(), used to authenticate the child process using the messages defined in the 
	 * CommuncationProtocol struct messages */
	void startCommunication();
	bool setReadPipeBlocking(bool);
	bool nonBlockingReturn();

	/* Common exception handler for all exceptions types to keep the same type when throwing */
	template <typename T>
	int exceptionHandler(const T &, const ReadFlags &);

	/* Pipes used by the main program to read stdin/stdout of forked process */
	Pipe readPipe, writePipe;

	/* These variables must be set to a positive value in order to use NonBlocking read. 
	 *  - delay represents the amount of time between two consecutive reads without reveiving data (returns -1 and
	 *  EAGAIN or EWOULDBLOCK).
	 *  - interval represents the maximum amount of failing reads before returning an exception or -1 and EINVAL.
	 * Delay 1000 and interval 1000000 will set an interval of 1s and will read every 0.1s after which will the data
	 * that was read, if any was read, otherwise an exception, if the exception flag is set or -1 and EINVAL. */
	static std::chrono::milliseconds defaultDelay, defaultInterval;
	static const int bufferSize = 1500;

	bool started = false;
	PipedProcess::Arguments arguments;
	PipeFlags readFlags;
#ifdef _WIN32
	/* Used so child processes die when parent dies (similar to Linux's prctl) */
	static HANDLE handlerJob;
	static JOBOBJECT_EXTENDED_LIMIT_INFORMATION jeli;
#endif
};

#endif /* PIPED_PROCESS_H */