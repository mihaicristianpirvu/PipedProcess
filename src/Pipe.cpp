#include <PipedProcess/Pipe.hpp>

PipeHandle Pipe::getReader() const {
	return this->pipe[Pipe::Read];
}

PipeHandle Pipe::getWriter() const {
	return this->pipe[Pipe::Write];
}

int Pipe::readNoSpecialCharacters(char *buffer, int size) const {
	int readSize, whiteSpaceCount = -1;

	while(true) {
		if( (readSize = this->read(buffer, size)) <= 0)
			return readSize;

		/* Count all the invalid characters at the beginning of the buffer */
		whiteSpaceCount = 0;
		for(int i=0; i<readSize && isspace(buffer[i]); i++)
			whiteSpaceCount++;

		/* If all the string is indeed a white space line, re-read it. */
		if(whiteSpaceCount >= readSize)
			continue;

		readSize -= whiteSpaceCount;
		/* Otherwise, if there are padding white space characters, move the string on top of them */
		if(whiteSpaceCount > 0)
			std::memmove(buffer, &buffer[whiteSpaceCount], readSize);
		buffer[readSize] = 0;

		/* Remove all the bad characters from the end */
		for(int i=readSize-1; i>=0 && isspace(buffer[i]); i--) {
			buffer[i] = '\0';
			--readSize;
		}

		if(readSize > 0)
			return readSize;
	}

	return -1;
}

Pipe::~Pipe() {
	assert(this->closePipe());
}