#include <PipedProcess/PipedProcess.hpp>

std::chrono::milliseconds PipedProcess::defaultDelay(100);
std::chrono::milliseconds PipedProcess::defaultInterval(std::chrono::seconds(5));

void PipedProcess::setArguments(int argc, char **argv) {
	this->arguments.argc = argc;
	this->arguments.argv = argv;
}

int PipedProcess::write(const std::string &message) {
	return this->writePipe.write(message.c_str(), message.size());
}

int PipedProcess::writeln(const std::string &message) {
	if(message.c_str()[message.size()-1] == '\n')
		return this->write(message);
	else {
		return this->write(message + "\n");
	}
}

void PipedProcess::startCommunication() {
	std::string word;
	word.reserve(100);

	/* Initial handshake protocol with the new process */
	assert(this->writeln(PipedProcess::CommunicationProtocol::handshakeInitialMessage) > 0);
	assert(this->read(word, 100, ReadFlags::Timer | ReadFlags::NoSpecialChars | ReadFlags::ThrowExceptions) > 0);
	assert(word == PipedProcess::CommunicationProtocol::handshakeAcceptMessage);
	assert(this->writeln(PipedProcess::CommunicationProtocol::ACKMessage) > 0);
}

void PipedProcess::printSpawning() {
	std::string str;
	assert(this->arguments.argv != nullptr);

	str = "Spawning: ";
	str.append(this->arguments.argv[0]).append(" - Arguments: ");
	if(this->arguments.argc == 1)
		str.append("None");
	else {
		for(int j=1; j<this->arguments.argc; j++)
			str.append(this->arguments.argv[j]).append(" ");
	}

	std::cerr<<str<<"\n";
}

int PipedProcess::read(std::string &word, size_t size, ReadFlags type,
	const std::chrono::milliseconds &interval, const std::chrono::milliseconds &delay) {
	char buffer[PipedProcess::bufferSize];
	int readSize;

	if( type & ReadFlags::NonBlocking )
		throw piped_process_exception("Non blocking mode not yet implemented correctly.");

	try {
		/* Blocking read */
		if(type & ReadFlags::Timer) {
			auto interval_left = interval;

			/* Make the socket a non blocking one */
			if(!this->setReadPipeBlocking(false))
				throw piped_process_exception("Error setting read flag", -1);

			while(true) {
				if(type & ReadFlags::NoSpecialChars)
					readSize = this->readPipe.readNoSpecialCharacters(buffer, size);
				else
					readSize = this->readPipe.read(buffer, size);

				/* If the read was unsuccesful, we need to check if no data was received becuase of non blocking read */
				if(readSize > 0)
					break;
				else {
					if(this->nonBlockingReturn()) {
						std::this_thread::sleep_for(delay);
						interval_left -= delay;
						if(interval_left.count() <= 0)
							throw out_of_time("Out if time", -1);
					}
					else {
						throw invalid_read("Invalid read", readSize);
					}
				}
			}

			/* Reset the socket to the default blocking state */
			if(!this->setReadPipeBlocking(true))
				throw piped_process_exception("Error setting read flag", -1);
		}
		else {
			if(type & ReadFlags::NoSpecialChars)
				readSize = this->readPipe.readNoSpecialCharacters(buffer, size);
			else
				readSize = this->readPipe.read(buffer, size);
			/* This is caught first by our catch to check if exceptions are enabled */
			if(readSize <= 0)
				throw invalid_read("Invalid read", readSize);
		}
	}
	catch (out_of_time &e) {
		return this->exceptionHandler<out_of_time>(e, type);
	}
	catch (invalid_read &e) {
		return this->exceptionHandler<invalid_read>(e, type);
	}
	catch (piped_process_exception &e) {
		return this->exceptionHandler<piped_process_exception>(e, type);
	}

	word.assign(buffer, readSize);
	return readSize;
}

template <typename T>
int PipedProcess::exceptionHandler(const T &exception, const ReadFlags &type) {
	/* Reset the socket to the default blocking state */
	if( (type & ReadFlags::Timer) || (type & ReadFlags::NonBlocking) ) {
		if(!this->setReadPipeBlocking(true)) {
			if(type & ReadFlags::ThrowExceptions)
				throw piped_process_exception("Error setting read flag", -1);
			else
				return -1;
		}
	}

	if(type & ReadFlags::ThrowExceptions)
		throw exception;
	else
		return exception.return_value;
}
