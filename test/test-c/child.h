#include <assert.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>

#define BUFSIZE 1024

void initialMessages();

/* IPC Protocol */
#define HANDSHAKE_INITIAL_MESSAGE "MAIN"
#define HANDSHAKE_ACCEPT_MESSAGE "ACCEPT"
#define ACK_MESSAGE "OK"

/* Writes to stdout and flushes */
#define flPrintf(...) do {		\
		printf(__VA_ARGS__);	\
		fflush(stdout);			\
	} while (0)

/* Compares 2 strings, based on the */
#define BSTRING_EQUALS(a, b) (strncmp((a), (b), strlen(b)) == 0)

/* Reads inside a while to avoid empty reads */
#define whRead(err, fd, buf, size) do {					\
	while (((err) = read((fd), (buf), (size))) == 0);	\
	assert((err) > 0);									\
} while(0)

/* Reads and then compares the read data with the cmp string */
#define readCompare(err, buf, size, cmp) do {		\
	whRead((err), STDIN_FILENO, (buf), (size));		\
	assert(BSTRING_EQUALS((buf), (cmp)));			\
} while (0)
