#include "child.h"

static char buffer[BUFSIZE];
static int rc;

void initialMessages() {
	fprintf(stderr, "Child was spawned\n");

	/* 3 way handshake to initialize the new process */
	readCompare(rc, buffer, BUFSIZE, HANDSHAKE_INITIAL_MESSAGE);
	flPrintf(HANDSHAKE_ACCEPT_MESSAGE"\n");
	readCompare(rc, buffer, BUFSIZE, ACK_MESSAGE);
}


int main(int argc, char **argv) {
	char buffer[BUFSIZE];
	int rc;

	initialMessages();

	/* Regular message */
	flPrintf("Message1");
	readCompare(rc, buffer, BUFSIZE, ACK_MESSAGE);

	/* Read blocking with a 1 second delay, using the default interval (5s) and exceptions */
	sleep(1);
	flPrintf("Message2");
	readCompare(rc, buffer, BUFSIZE, ACK_MESSAGE);

	/* Read blocking with a 1 second delay, using a custom interval (0.5s) and no exceptions */
	sleep(1);
	readCompare(rc, buffer, BUFSIZE, ACK_MESSAGE);

	/* Read blocking with a 1 second delay, using the default interval (5s) and exceptions */
	sleep(1);
	flPrintf("Message4");
	readCompare(rc, buffer, BUFSIZE, ACK_MESSAGE);

	/* Read blocking with a 1 second delay, using a custom interval (0.5s) and exceptions */
	sleep(1);
	flPrintf("Message5");
	readCompare(rc, buffer, BUFSIZE, ACK_MESSAGE);

	return 0;
}