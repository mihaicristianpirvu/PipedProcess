cmake_minimum_required (VERSION 2.8.0)
project (PipedProcess)

include_directories(src/include)
FILE(GLOB_RECURSE SRC_FILES src/*.cpp)
add_library(pipedprocess ${SRC_FILES})
